from selenium import webdriver
from selenium.webdriver.support.ui import Select
import time

browser = webdriver.Chrome()
link = "http://suninjuly.github.io/selects2.html"
browser.get(link)
x1 = browser.find_element_by_css_selector('[id="num1"]').text
x2 = browser.find_element_by_css_selector('[id="num2"]').text

select = Select(browser.find_element_by_tag_name("select"))
select.select_by_value(str(int(x1) + int(x2)))  # ищем элемент


option2 = browser.find_element_by_css_selector("[type='submit']")
option2.click()
time.sleep(10)
browser.quit()


