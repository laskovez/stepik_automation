import os
from selenium import webdriver
import time
import math

browser = webdriver.Chrome()
link = "http://suninjuly.github.io/file_input.html"
browser.get(link)

current_dir = os.path.abspath(os.path.dirname(__file__))    # получаем путь к директории текущего исполняемого файла
file_path = os.path.join(current_dir, 'file.txt')           # добавляем к этому пути имя файла

input1 = browser.find_element_by_name("firstname")
input1.send_keys("Serhii")
input2 = browser.find_element_by_name("lastname")
input2.send_keys("Liaskovets")
input3 = browser.find_element_by_name("email")
input3.send_keys("lz@gmail.com")
input4 = browser.find_element_by_name("file")
input4.send_keys(file_path)
button1 = browser.find_element_by_tag_name("button")
button1.click()
